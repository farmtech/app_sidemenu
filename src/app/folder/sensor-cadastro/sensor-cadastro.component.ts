
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SensorService } from '../sensor.service';


@Component({
  selector: 'app-sensor-cadastro',
  templateUrl: './sensor-cadastro.component.html',
  styleUrls: ['./sensor-cadastro.component.scss'],
})
export class SensorCadastroComponent implements OnInit {

  sensorForm: FormGroup;

  constructor(private fb: FormBuilder, private sensorService: SensorService) {
      this.sensorForm = this.fb.group({
          descricao: ['', [Validators.required]],
          localizacao: ['', [Validators.required]]
      });
  }

  ngOnInit() { }


  saveSensor() {
      if (this.sensorForm.valid) {
          this.sensorService.saveSensor(this.sensorForm.value);
          this.sensorForm.reset();
      }
  }

}

