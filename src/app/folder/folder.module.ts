import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FolderPageRoutingModule } from './folder-routing.module';

import { FolderPage } from './folder.page';
import { SensorCadastroComponent } from './sensor-cadastro/sensor-cadastro.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SensorListagemComponent } from './sensor-listagem/sensor-listagem.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FolderPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [FolderPage, 
    SensorCadastroComponent, 
    CadastroComponent, 
    SensorListagemComponent,
    HomeComponent,
  ]
})
export class FolderPageModule {}
