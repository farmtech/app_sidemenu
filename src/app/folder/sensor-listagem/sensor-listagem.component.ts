import { Component, OnInit } from '@angular/core';
import { Sensor } from '../sensor-cadastro/sensor';
import { SensorService } from '../sensor.service';

@Component({
  selector: 'app-sensor-listagem',
  templateUrl: './sensor-listagem.component.html',
  styleUrls: ['./sensor-listagem.component.scss'],
})
export class SensorListagemComponent implements OnInit {
  sensores: Sensor[] = [];

  constructor(private sensorService: SensorService) {}

  ngOnInit() {
    this.sensores = this.sensorService.getSensores();
  }
}