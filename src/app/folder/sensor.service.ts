import { Injectable } from '@angular/core';
import { Sensor } from './sensor-cadastro/sensor';

@Injectable({
  providedIn: 'root'
})
export class SensorService {

  private storageKey = 'sensores';

  constructor() { }

  public saveSensor(sensor: Sensor) {
      const sensores = this.getSensores();
      sensores.push(sensor);
      localStorage.setItem(this.storageKey, JSON.stringify(sensores));
  }

  public getSensores(): Sensor[] {
      const sensores = localStorage.getItem(this.storageKey);
      return sensores ? JSON.parse(sensores) : [];
  }

}
