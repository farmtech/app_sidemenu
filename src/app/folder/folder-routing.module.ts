import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FolderPage } from './folder.page';
import { SensorCadastroComponent } from './sensor-cadastro/sensor-cadastro.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { SensorListagemComponent } from './sensor-listagem/sensor-listagem.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'sensor',
    component: SensorCadastroComponent
  },
  {
    path: 'sensores',
    component: SensorListagemComponent
  },
  {
    path: 'cadastro',
    component: CadastroComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FolderPageRoutingModule {}
