import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/folder', icon: 'mail' },
    { title: 'Cadastro', url: '/folder/cadastro', icon: 'mail' },
    { title: 'Novo Sensor', url: '/folder/sensor', icon: 'warning' },
    { title: 'Lista de Sensores', url: '/folder/sensores', icon: 'warning' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders', 'Aula'];
  constructor() {}
}
